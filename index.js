'use strict';

module.exports = {
  name: require('./package').name,
  contentFor(type, config) {
    if (type === 'head') {
      return '<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />'
      + '<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">';
    } else if (type === 'head-footer') {
      return '';
    } else if (type === 'body') {
      return '';
    } else if (type === 'body-footer') {
      return '';
    };
  },

  included: function(app) {
    // Core JS Files
    app.import('vendor/material-dashboard-pro-html-v2.1.0/assets/js/core/jquery.min.js');
    app.import('vendor/material-dashboard-pro-html-v2.1.0/assets/js/core/popper.min.js');
    app.import('vendor/material-dashboard-pro-html-v2.1.0/assets/js/core/bootstrap-material-design.min.js');
    app.import('vendor/material-dashboard-pro-html-v2.1.0/assets/js/plugins/perfect-scrollbar.jquery.min.js');

    // Plugin for the momentJs
    app.import('vendor/material-dashboard-pro-html-v2.1.0/assets/js/plugins/moment.min.js');

    // Plugin for Sweet Alert
    app.import('vendor/material-dashboard-pro-html-v2.1.0/assets/js/plugins/sweetalert2.js');

    // Forms Validations Plugin
    app.import('vendor/material-dashboard-pro-html-v2.1.0/assets/js/plugins/jquery.validate.min.js');

    // Plugin for the Wizard, full documentation here: https://github.com/VinceG/twitter-bootstrap-wizard
    app.import('vendor/material-dashboard-pro-html-v2.1.0/assets/js/plugins/jquery.bootstrap-wizard.js');

    // Plugin for Select, full documentation here: http://silviomoreto.github.io/bootstrap-select
    app.import('vendor/material-dashboard-pro-html-v2.1.0/assets/js/plugins/bootstrap-selectpicker.js');

    // Plugin for the DateTimePicker, full documentation here: https://eonasdan.github.io/bootstrap-datetimepicker/
    app.import('vendor/material-dashboard-pro-html-v2.1.0/assets/js/plugins/bootstrap-datetimepicker.min.js');

    // DataTables.net Plugin, full documentation here: https://datatables.net/
    app.import('vendor/material-dashboard-pro-html-v2.1.0/assets/js/plugins/jquery.dataTables.min.js');

    // Plugin for Tags, full documentation here: https://github.com/bootstrap-tagsinput/bootstrap-tagsinputs
    app.import('vendor/material-dashboard-pro-html-v2.1.0/assets/js/plugins/bootstrap-tagsinput.js');

    // Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput
    app.import('vendor/material-dashboard-pro-html-v2.1.0/assets/js/plugins/jasny-bootstrap.min.js');

    // Full Calendar Plugin, full documentation here: https://github.com/fullcalendar/fullcalendar
    app.import('vendor/material-dashboard-pro-html-v2.1.0/assets/js/plugins/fullcalendar.min.js');

    // Vector Map plugin, full documentation here: http://jvectormap.com/documentation/
    app.import('vendor/material-dashboard-pro-html-v2.1.0/assets/js/plugins/jquery-jvectormap.js');

    // Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/
    app.import('vendor/material-dashboard-pro-html-v2.1.0/assets/js/plugins/nouislider.min.js');

    // Include a polyfill for ES6 Promises (optional) for IE11, UC Browser and Android browser support SweetAlert
    // <script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>

    // Library for adding dinamically elements
    app.import('vendor/material-dashboard-pro-html-v2.1.0/assets/js/plugins/arrive.min.js');

    // Google Maps Plugin
    // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>

    // Chartist JS
    app.import('vendor/material-dashboard-pro-html-v2.1.0/assets/js/plugins/chartist.min.js');

    // Notifications Plugin
    app.import('vendor/material-dashboard-pro-html-v2.1.0/assets/js/plugins/bootstrap-notify.js');

    // Control Center for Material Dashboard: parallax effects, scripts for the example pages etc
    // <script src="../assets/js/material-dashboard.js?v=2.1.0" type="text/javascript"></script>

    app.import('vendor/material-dashboard-pro-html-v2.1.0/assets/js/material-dashboard.js');
  }
};
